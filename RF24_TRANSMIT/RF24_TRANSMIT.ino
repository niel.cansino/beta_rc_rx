/*
* Arduino Wireless Communication Tutorial
*     Example 1 - Transmitter Code
*                
* by Dejan Nedelkovski, www.HowToMechatronics.com
* 
* Library: TMRh20/RF24, https://github.com/tmrh20/RF24/
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";

union uUINTTOBYTE{
  uint8_t BYTE[4];
  uint32_t UINT32;
}; 

const int X_pin = 0; // analog pin connected to X output
const int Y_pin = 1; // analog pin connected to Y output

void pin2Toggle(){
  static uint8_t pinVal = 0x0;

  // toggle pin
   pinVal ^= 1;

 // output to GPIO
   digitalWrite(2, pinVal);
  
}

void setup() {
  Serial.begin(115200);
  radio.begin();
  radio.setRetries(4, 2);
  //radio.setDataRate(RF24_1MBPS);
  radio.openWritingPipe(address);
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
}
void loop() {
  static uUINTTOBYTE myval;
//  const char text[] = "Hello World";
  digitalWrite(2, 0x1);
  myval.UINT32 = analogRead(Y_pin);
  radio.write(&myval.BYTE, sizeof(myval));
  digitalWrite(2, 0x0);
  
  
  //pin2Toggle();
  //Serial.print("tx ");
  Serial.println(myval.UINT32);
  delay(5);
  
  //myval.UINT32++;
}
