#include <Arduino.h>

/*
* Arduino Wireless Communication Tutorial
*       Example 1 - Receiver Code
*                
* by Dejan Nedelkovski, www.HowToMechatronics.com
* 
* Library: TMRh20/RF24, https://github.com/tmrh20/RF24/
*/
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7, 8); // CE, CSN
const byte address[6] = "00001";

int PWMControl = 3;

union uUINTTOBYTE{
  uint8_t BYTE[4];
  uint32_t UINT32;
}; 

void pin2Toggle(){
  static uint8_t pinVal = 0x0;

  // toggle pin
   pinVal ^= 1;

 // output to GPIO
   digitalWrite(2, pinVal);
  
}

void setup() {
  pinMode(PWMControl, OUTPUT);
  TCCR2B = (TCCR2B & 0b11111000) | 0x06;

  Serial.begin(115200);
  radio.begin();
  //radio.setDataRate(RF24_1MBPS);
  radio.openReadingPipe(0, address);
  radio.setPALevel(RF24_PA_MIN);
  radio.startListening();

}

void loop() {
  static uUINTTOBYTE myval;
  static int calcval, pwmval;
  if (radio.available()) {
    uint8_t val[32] = "";
    digitalWrite(2, 0x1);
    radio.read(&myval.BYTE, sizeof(myval));
    digitalWrite(2, 0x0);
    //pin2Toggle();
    //Serial.print("rx ");
    
    calcval = 512 - myval.UINT32;
    if(calcval < 0) calcval = 0;
    Serial.println(myval.UINT32);

    pwmval = map(calcval, 0, 512, 25, 50);
    analogWrite(PWMControl, pwmval);
    

  }

}
