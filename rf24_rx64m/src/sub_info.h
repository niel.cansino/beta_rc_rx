/*!
 *  
 *  @file    
 *  @brief   Speeder submodule definition file
 *  @author  Niel.Cansino
 *  @details 
 */
#ifndef __SUB_INFO_H__
#define __SUB_INFO_H__

//uncomment the Submodule name. Do not comment more than 1!
//#define SUBMODULE_GPU
//#define SUBMODULE_ADU
//#define SUBMODULE_EMU
//#define SUBMODULE_UOI
//#define SUBMODULE_PSC
//#define SUBMODULE_HSU
//#define SUBMODULE_FLU
//#define SUBMODULE_RLU
//#define SUBMODULE_SPC
#define SUBMODULE_RCU

//----Submodule board number----
#define SUBMODULE_BOARD_NUM  (1)

//###################################################################
// Submodule Name Declaration
//###################################################################
#ifdef SUBMODULE_GPU
#define THIS_SUB_NAME   "GPU"
#endif
#ifdef SUBMODULE_ADU
#define THIS_SUB_NAME   "ADU"
#endif
#ifdef SUBMODULE_EMU
#define THIS_SUB_NAME   "EMU"
#endif
#ifdef SUBMODULE_UOI
#define THIS_SUB_NAME   "UOI"
#endif
#ifdef SUBMODULE_PSC
#define THIS_SUB_NAME   "PSC"
#endif
#ifdef SUBMODULE_HSU
#define THIS_SUB_NAME   "HSU"
#endif
#ifdef SUBMODULE_FLU
#define THIS_SUB_NAME   "FLU"
#endif
#ifdef SUBMODULE_RLU
#define THIS_SUB_NAME   "RLU"
#endif
#ifdef SUBMODULE_SPC
#define THIS_SUB_NAME   "SPC"
#endif
#ifdef SUBMODULE_RCU
#define THIS_SUB_NAME   "RCU"
#endif
//###################################################################
// Submodule general macro definitions
//###################################################################
#define DEBUG_MODE                  (1)     //----During debugging mode----
#define SYNC_LOOP_RATE_NOMINAL      (100)   //----Sync Loop Nominal frequency----
#define FAST_LOOP_RATE_NOMINAL      (10000) //----Fast loop nominal rate----
#define FAST_LOOP_RATE_NOMINAL2     (500)   //----Fast loop nominal rate 2----

//###################################################################
// Submodule specific macro definitions
//###################################################################
#ifdef SUBMODULE_RCU

#endif

//###################################################################
// Include files
//###################################################################
#include "SP_typedef.h"

//###################################################################
// Global variables
//###################################################################
// --Empty here--

//###################################################################
// 
//###################################################################

#endif