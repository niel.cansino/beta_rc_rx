/*!
 *  
 *  @file    
 *  @brief   main function for beta_ADU
 *  @details 
 */
//###################################################################
// Include files
//###################################################################
#include "sub_info.h"
#include "SP_typedef.h"
#include "SP_Led.h"
#include "SP_Serial.h"
#include "SP_Synchronizer.h"
#include "SP_DiagTool.h"
#include "SP_Imu.h"
#include "SP_Fcu_If.h"
#include "SP_Timer.h"
#include "SP_Select.h"
#include "SP_DataBankImu.h"
extern "C"{
#include <stdio.h>
#include <stdbool.h>
#include "SP_utility.h"
#include "iodefine.h"
#include "r_bsp_cpu.h"
}
//###################################################################
// Macro
//###################################################################

#define BENCH_TEST              (0)
#define DO_DEBUG                if(BENCH_TEST==1)
#define IMU_INACTIVE_LIMIT      (200)

#define SET_STATE(state)            gCurrentState = state
#define IS_CURRENT_STATE(state)     if(state == gCurrentState)
#define CURRENT_STATE               gCurrentState
#define RUN_STATE_ROUTINE           StateMap[gCurrentState]()

//----Macros for downsampling----
#define ROUTINE_1HZ(count, base_freq)   if(0 == count % (base_freq))
#define ROUTINE_10HZ(count, base_freq)  if(0 == count % (base_freq/10))
#define ROUTINE_100HZ(count, base_freq) if(0 == count % (base_freq/100))

//----Software Reset Routine----
#define SOFTWARE_RESET              {   R_BSP_RegisterProtectDisable(BSP_REG_PROTECT_LPC_CGC_SWR); \
                                        SYSTEM.SWRR = 0xA501; \
                                        R_BSP_RegisterProtectEnable(BSP_REG_PROTECT_LPC_CGC_SWR); \
                                    }

//----Deassert all change flags in System Register
#define SYSREG_DEASSERT_ALL_FLAGS   {   for(uint8_t i=0; i<sizeof(gSystemRegister.SYSREGSHCF.BYTE)/sizeof(gSystemRegister.SYSREGSHCF.BYTE[0]); ++i){ \
                                            gSystemRegister.SYSREGSHCF.BYTE[i] = 0;\
                                        }\
                                        for(uint8_t i=0; i<sizeof(gSystemRegister.SYSREGFXCF.BYTE)/sizeof(gSystemRegister.SYSREGFXCF.BYTE[0]); ++i){ \
                                            gSystemRegister.SYSREGFXCF.BYTE[i] = 0;\
                                        }\
                                    }
#define SYSREG_ASSERT_ALL_FX_FLAGS  {   for(uint8_t i=0; i<sizeof(gSystemRegister.SYSREGFXCF.BYTE)/sizeof(gSystemRegister.SYSREGFXCF.BYTE[0]); ++i){ \
                                            gSystemRegister.SYSREGFXCF.BYTE[i] = 0xFF;\
                                        }\
                                    }
//###################################################################
// Type definition
//###################################################################
//----State enumeration----
enum eMyStates {
    ST_NONE = 0,
    ST_SETUP,
    ST_WAIT_CONNECT,
    ST_STANDBY,
    ST_MEASURING
};

//###################################################################
// Global value
//###################################################################
//---- Global class objects ----
SP_Led          led;            //----LED Indicators----
SP_Serial       *diag_serial;   //----For message display----
SP_DiagTool     diag;           //----Diagnostics Library----
SP_Synchronizer sync;           //----Synchronization Library----
SP_Fcu_If       fcu;            //----FCU Comms API Library----
SP_Select       testdata_sw;    //----Board ID switch used as test data switch (only for ADU)----
SP_Imu          imu;            //----IMU I/F Library----
SP_Timer        timer;          //----Timer library----
SP_DataBankImu  imubuffer;      //----IMU buffer----

//----Global variables----
uint16_t        gSynchroCount;      //----Synchronization count for downsampling sync frequency----
eMyStates       gCurrentState;      //----Current program state----
typImuData      gMyImuData;         //----variable to hold IMU data----
uint16_t        gTickCount;         //----counter for downsampling loop frequency----
typSysReg       gSystemRegister;    //----SYSTEM REGISTER----

const char *state_name[] = {
    "none\n",
    "setup\n",
    "wait_connect\n",
    "standby\n",
    "measuring\n"
};

//###################################################################
// Function prototypes
//###################################################################
void setup(void);
void init_sys_reg(typSysReg &iSysReg);
void system_chk(void);
void loop(void);
void diagnosis(void);
void imuTestData(typImuData &oImuData, const uint16_t iCount);

//----Prototypes of state functions----
void st_wait_connect(void);
void st_standby(void);
void st_measuring(void);

//----State table----
static void (*StateMap[])(void) = {
    st_standby,     	//----placeholder for ST_NONE----
    st_standby,		//----placeholder for ST_SETUP----
    st_wait_connect,
    st_standby,
    st_measuring
};

/*!------------------------------------------------------------------
@fn         
@brief      Main loop
@details    Uses default main loop
-------------------------------------------------------------------*/
void main(void){
    setup();
    while(1){
        system_chk();
        loop();
        diagnosis();
    }
}

/*!------------------------------------------------------------------
@fn         
@brief      Setup codes
@details    
-------------------------------------------------------------------*/
void setup(){

    //----Set state----
    SET_STATE(ST_SETUP);
    
    //----Initialize system register----
    init_sys_reg(gSystemRegister);
    
    //----Setup LEDs----
    led.Begin();

    //----Setup test data switch----
    testdata_sw.Begin();

    //----Setup sync according to settings in Sync_Mode_----
    sync.Begin();
    switch(gSystemRegister.SYSREGSH.REG.SYNC){
        case SP_SYNCHRONIZER_SELF_GEN:
            sync.EnterSelfGeneration(SYNC_LOOP_RATE_NOMINAL);
            break;
        case SP_SYNCHRONIZER_TRIGGERED:
            sync.EnterSynchronizedTriger();
            break;
        case SP_SYNCHRONIZER_SLEEP:
            //----Do nothing----
            break;
    }

    //----Setup Timer----
    timer.Begin(FAST_LOOP_RATE_NOMINAL); //@1Fast loop rate nominal

    //----Setup Diagnostics tool----
    diag.Begin();
    diag_serial = &diag.sp;

    //----Setup FCU API Library----
    fcu.Begin();

    //----Setup IMU API Library----
    imu.Begin();
    imu.SetTestEnable(0);

    //----Setup IMU Buffer Library----
    imubuffer.Begin();

    //----Set Setup LED----
    led.SetBit0(0x1);

    //----Enter next state----
    SET_STATE(ST_WAIT_CONNECT);
}

/*!------------------------------------------------------------------
@fn         
@brief      Loop() routine
@details    
-------------------------------------------------------------------*/
void loop(void){
    static uint16_t     failed = 0;
    static bool         tosendstate         = false;
    static bool         send_fcu_data       = false;

    //----IMU Measure----
    if( imu.Run() ){

        //----Toggle bit 6----
        led.ToggleBit6();
    
        //----get data once ready----
        imu.GetImuData(gMyImuData);

        //----push to circular buffer if measurement is ON----
        IS_CURRENT_STATE(ST_MEASURING){
            imubuffer.Push(gMyImuData);

        }
    }

    //----Update input from CAN FCU via sync----
    if(sync.Start()){

        //----Get synchro count----
        gSynchroCount = sync.GetSynchroCount();

        //----Update FCU API----
        fcu.UpdateRxBuffer();

        //----check if c-code has arrived----
        if(fcu.CCodeArrived()){

            tosendstate = true;
        
        }

        //----Send the IMU data to FCU only during these states----
        IS_CURRENT_STATE(ST_MEASURING){
        
            if (imubuffer.Get(gMyImuData)) {
                //----toggle ready bit----
                send_fcu_data = true;
            }
        }
        
        //----10Hz loop for LED sync monitoring----
        ROUTINE_10HZ(gSynchroCount, SYNC_LOOP_RATE_NOMINAL){
            
            //----Update LED sync_loop Indicator----
            led.ToggleBit2();
        }
    
    }
    
    //----Main loop running @ speed controlled by the UART reader----
    if(timer.Tick()){

        //----Get fast loop tick count----
        gTickCount = timer.GetTickCount();

        //----Execute routine of current_state----
        RUN_STATE_ROUTINE;

        //----Conduct state-agnostic periodic routine (error handling, LED flashing, etc)----
        //----1Hz loop----
        ROUTINE_1HZ(gTickCount, FAST_LOOP_RATE_NOMINAL){
               DO_DEBUG{printf("%s\n", state_name[gCurrentState]);}
        }

        //----10Hz loop for monitoring this fast loop----
        ROUTINE_10HZ(gTickCount, FAST_LOOP_RATE_NOMINAL){
            
            //----Update LED fast_loop Indicator----
            led.ToggleBit1();
        }

        //----Do CAN transmission here----
        //----This shoud always be after the processing loop----
        if(tosendstate){

            fcu.SendState(CURRENT_STATE, gSynchroCount);
            tosendstate = false;
        
        }

        //----Send FCU data----
        if(send_fcu_data){

            //----send test data when the ff. condition is true----
            //----Otherwise the program wuill send the actual data----
            if(0x0 == testdata_sw.GetModuleNumber()){
                
                //----send test data----
                imuTestData(gMyImuData, gSynchroCount);

            }
            fcu.SendIMUData(gMyImuData, gSynchroCount);

            send_fcu_data = false;
        }
        //----set/reset test data mode LED indicator----
        led.SetBit7((0x0 == testdata_sw.GetModuleNumber()) ? 0x1 : 0x0);
    }
}
/*!------------------------------------------------------------------
@fn         
@brief      
@details    
-------------------------------------------------------------------*/
void diagnosis(void){
    //----Parse UART messages here----
    diag.Run();

}

/*!------------------------------------------------------------------
@fn         
@brief      
@details    
-------------------------------------------------------------------*/
void system_chk(void){
    //----Check system register----
    if(fcu.ReadSystemMsg(gSystemRegister)){        

        //----Reset----
        if(gSystemRegister.SYSREGSHCF.REG.RST && (0xD6 == gSystemRegister.SYSREGSH.REG.RST)){ 

            DO_DEBUG{printf("reset rcvd\n");}
            SOFTWARE_RESET;

        }

        //----Sync----
        if(gSystemRegister.SYSREGSHCF.REG.SYNC){

            DO_DEBUG{printf("received sync cgh req: %d\n", gSystemRegister.SYSREGSH.REG.SYNC);}
            sync.ChangeSyncMode((eSyncMode)gSystemRegister.SYSREGSH.REG.SYNC, SYNC_LOOP_RATE_NOMINAL);      
            
        }

        //----Deassert all flags----
        SYSREG_DEASSERT_ALL_FLAGS;

    }
}

/*!------------------------------------------------------------------
@fn         
@brief      
@details    Initialize system register here
            This is a submodule-specific routine
-------------------------------------------------------------------*/
void init_sys_reg(typSysReg &iSysReg){

    //----Zero the mask bits----
    SYSREG_DEASSERT_ALL_FLAGS;
#if BENCH_TEST==1
    gSystemRegister.SYSREGSH.REG.SYNC       = SP_SYNCHRONIZER_SELF_GEN;    //Self for now
#else
    gSystemRegister.SYSREGSH.REG.SYNC       = SP_SYNCHRONIZER_TRIGGERED;    //trigger
#endif

}
//===================================================================================

/*!------------------------------------------------------------------
@fn         
@brief      
@details    
-------------------------------------------------------------------*/
void st_wait_connect(void){

    //----move to standby when a connect() request from FCU has arrived----
    if(fcu.ToConnect()){

        //----State transition to standby----
        SET_STATE(ST_STANDBY);

    }
}

/*!------------------------------------------------------------------
@fn         
@brief      
@details    
-------------------------------------------------------------------*/
void st_standby(void){

    //----wait for measure() command----
    if(true == fcu.ToStartMeasure()){

        SET_STATE(ST_MEASURING);
    
    }
}

/*!------------------------------------------------------------------
@fn         
@brief      
@details    Measuring routine
            Only use for state transition for
-------------------------------------------------------------------*/
void st_measuring(void){

    //----Exit measuring mode----
    if(true == fcu.ToEndMeasure()){

        SET_STATE(ST_STANDBY);

    }
}

void imuTestData(typImuData &oImuData, const uint16_t iCount){
    static float16_t fCount;

    fCount = (float16_t)iCount;
    // Pass the data to the caller
    oImuData.QuaternionW = 1 + fCount*0.000001;
    oImuData.QuaternionX = 2 - fCount*0.000001;
    oImuData.QuaternionY = 3 + fCount*0.000001;
    oImuData.QuaternionZ = 4 - fCount*0.000001;
    oImuData.GyroP = 5 + fCount*0.001;
    oImuData.GyroQ = fCount*0.001;
    oImuData.GyroR = -5 + fCount*0.001;
    oImuData.AccelX  = 3 + fCount*0.002;
    oImuData.AccelY  = fCount*0.001;
    oImuData.AccelZ  = -3 + fCount*0.002;
    oImuData.EulerAngleRoll = 2 + fCount*0.00001;;
    oImuData.EulerAnglePitch = fCount*0.00001;
    oImuData.EulerAngleYaw = -2 + fCount*0.00001;
    oImuData.Altitude = fCount*0.000001;
    oImuData.SpeedNoath = fCount*0.000001;
    oImuData.SpeedEast = fCount*0.000001;
    oImuData.SpeedDown = fCount*0.000001;
    oImuData.RawGyroP = 5 + fCount*0.001;
    oImuData.RawGyroQ = fCount*0.001;
    oImuData.RawGyroR = -5 + fCount*0.001;
    oImuData.RawAccelX  = 3 + fCount*0.002;
    oImuData.RawAccelY  = fCount*0.001;
    oImuData.RawAccelZ  = -3 + fCount*0.002;
}

#ifdef __cplusplus
void abort(void){

}
#endif
